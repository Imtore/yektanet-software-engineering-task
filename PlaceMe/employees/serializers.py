from rest_framework import serializers

from PlaceMe.employees.models import EmployeeProfile, Resume
from PlaceMe.users.models import CustomUser


class EmployeeProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmployeeProfile
        fields = ['id', 'owner', 'first_name', 'last_name', 'age', 'gender']


class ResumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resume
        fields = ['id', 'file']

    def create(self, validated_data):
        file = validated_data.pop('file')
        owner = CustomUser.objects.get(pk=self.context['owner_id'])
        resume = Resume.objects.create(owner=owner, file=file)
        return resume



