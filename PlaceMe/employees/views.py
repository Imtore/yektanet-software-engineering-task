from django.core.cache import cache

from rest_framework import permissions
from rest_framework.generics import RetrieveUpdateAPIView, ListAPIView, DestroyAPIView, CreateAPIView, ListCreateAPIView
from rest_framework.parsers import FileUploadParser
from rest_framework import status
from rest_framework.response import Response

from PlaceMe.employees.models import EmployeeProfile, Resume
from PlaceMe.employees.serializers import EmployeeProfileSerializer, ResumeSerializer
from PlaceMe.notices.serializers import NoticesDetailSerializer
from PlaceMe.permissions import IsOwnerOrReadOnly, IsEmployeeOrReadOnly, IsEmployee
from PlaceMe.notices.models import RecruitmentNotice
from PlaceMe.users.models import CustomUser


class EmployeeProfileDetailView(RetrieveUpdateAPIView):
    queryset = EmployeeProfile.objects.all()
    serializer_class = EmployeeProfileSerializer
    permission_classes = [IsOwnerOrReadOnly]


class EmployeeProfilesListView(ListAPIView):
    queryset = EmployeeProfile.objects.all()
    serializer_class = EmployeeProfileSerializer
    permission_classes = [permissions.IsAuthenticated]


class ResumeDetailView(DestroyAPIView):
    queryset = Resume.objects.all()
    permission_classes = [IsOwnerOrReadOnly]
    serializer_class = ResumeSerializer


class SubmitResumeView(CreateAPIView):
    parser_class = (FileUploadParser,)
    permission_classes = [IsEmployeeOrReadOnly]

    def post(self, request, *args, **kwargs):
        serializer = ResumeSerializer(data=request.data, context={"owner_id": request.user.id})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


class RelatedNoticesView(ListAPIView):
    permission_classes = [IsEmployee]
    serializer_class = NoticesDetailSerializer

    def get_queryset(self):
        user_key = str(self.request.user)
        user = cache.get(user_key)
        if user is None:
            print("test1")
            user = CustomUser.objects.get(id=self.request.user.id)
            cache.set(user_key, user, 7200)

        career_field_objs = user.career_fields.all()
        career_fields = []
        for career_field in career_field_objs:
            career_fields.append(career_field.field)
        career_fields = sorted(career_fields)
        query_key = ', '.join(map(str, career_fields))
        notices = cache.get(query_key)
        if notices is None:
            print("test2")
            notices = RecruitmentNotice.objects.raw("select count(d.id), d.id, d.title, d.expiration_date, d.start_time, "
                                                    "d.end_time, d.salary, d.owner_id from (select b.careerfield_id as "
                                                    "careerfield_id from users_customuser as a INNER JOIN "
                                                    "users_customuser_career_fields as b on a.id = b.customuser_id where a.id=%s) as c inner join "
                                                    "(notices_recruitmentnotice as a INNER JOIN "
                                                    "notices_recruitmentnotice_career_fields nrcf on a.id = "
                                                    "nrcf.recruitmentnotice_id) as d on c.careerfield_id = d.careerfield_id group "
                                                    "by d.id order by count(d.id) DESC", [self.request.user.id])
            cache.set(query_key, notices, 7200)

        return notices
