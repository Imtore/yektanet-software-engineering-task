from django.urls import path
from PlaceMe.employees.views import *

urlpatterns = [
    path('profile/<int:pk>', EmployeeProfileDetailView.as_view()),
    path('profiles', EmployeeProfilesListView.as_view()),
    path('resume/upload', SubmitResumeView.as_view()),
    path('resume/<int:pk>', ResumeDetailView.as_view()),
    path('opportunities', RelatedNoticesView.as_view()),
]
