from django.db import models

from PlaceMe.users.models import CustomUser, CareerField

GENDERS = [('M', 'male'), ('F', 'female')]


class EmployeeProfile(models.Model):
    owner = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, related_name="employee_account"
    )

    username = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    age = models.PositiveIntegerField(null=False, blank=False)
    gender = models.CharField(choices=GENDERS, max_length=1)


class Resume(models.Model):
    owner = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, related_name="resume"
    )

    profile = models.OneToOneField(
        EmployeeProfile, on_delete=models.CASCADE, related_name="employee_resume", null=True)

    file = models.FileField(null=True, blank=True)

    def __str__(self):
        return self.file.name
