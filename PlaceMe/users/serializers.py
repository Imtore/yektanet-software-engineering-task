from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist

from PlaceMe.users.models import CustomUser, CareerField
from PlaceMe.employers.models import EmployerProfile
from PlaceMe.employees.models import EmployeeProfile


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProfile
        fields = ['username', 'first_name', 'last_name', 'age', 'gender']


class EmployeeRegistrationSerializer(serializers.ModelSerializer):
    profile = EmployeeSerializer(required=False)

    class Meta:
        model = CustomUser
        fields = ('email', 'password', 'role', 'profile')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = CustomUser.objects.create_user(**validated_data)
        EmployeeProfile.objects.create(
            owner=user,
            username=profile_data['username'],
            first_name=profile_data['first_name'],
            last_name=profile_data['last_name'],
            age=profile_data['age'],
            gender=profile_data['gender']
        )
        return user


class EmployerSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployerProfile
        fields = ['company_name', 'established_year', 'address', 'phone_number']


class EmployerRegistrationSerializer(serializers.ModelSerializer):
    profile = EmployerSerializer(required=False)

    class Meta:
        model = CustomUser
        fields = ('email', 'password', 'role', 'profile')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = CustomUser.objects.create_user(**validated_data)
        EmployerProfile.objects.create(
            owner=user,
            company_name=profile_data['company_name'],
            established_year=profile_data['established_year'],
            address=profile_data['address'],
            phone_number=profile_data['phone_number'],

        )
        return user


class CareerFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = CareerField
        fields = ['field']


def update_career_fields(instance, career_fields):
    old_set = set()
    for existing in instance.career_fields.all():
        old_set.add(existing.id)

    new_set = set()
    for career_field_data in career_fields:
        new_set.add(career_field_data.get('field'))

    # delete the careers we don't want anymore
    deletion_set = old_set.difference(new_set)
    for old in deletion_set:
        instance.save()
        career_field = CareerField.objects.get(field=old)
        career_field.save()
        instance.career_fields.remove(career_field)

    addition_set = new_set.difference(old_set)
    for new in addition_set:
        instance.save()
        try:
            career_field = CareerField.objects.get(field=new)
        except ObjectDoesNotExist:
            career_field = CareerField.objects.create(field=new)
        instance.career_fields.add(career_field)
    return instance


class UserCareerSerializer(serializers.ModelSerializer):
    career_fields = CareerFieldSerializer(many=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'career_fields']

    def update(self, instance, validated_data):
        career_fields = validated_data.pop('career_fields')
        instance = super().update(instance, validated_data)
        instance = update_career_fields(instance, career_fields)
        return instance

