from django.urls import path

from PlaceMe.users.views import *

urlpatterns = [
    path('careers/<int:pk>', CareerFieldView.as_view()),
]