from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager

ROLES = [('Er', 'employer'), ('Ee', 'employee')]


class CareerField(models.Model):
    IT_PROGRAMMING = 1
    ARCHITECTURE_ENGINEERING = 2
    ARTS_CULTURE_ENTERTAINMENT = 3
    BUSINESS_MANAGEMENT_ADMINISTRATION = 4
    COMMUNICATIONS = 5
    COMMUNITY_SOCIALSERVICES = 6
    EDUCATION = 7
    SCIENCE_TECHNOLOGY = 8
    INSTALLATION_REPAIR_MAINTENANCE = 9
    FARMING_FISHING_FORESTRY = 10
    GOVERNMENT = 11
    HEALTH_MEDICINE = 12
    LAW_PUBLICPOLICY = 13
    SALES = 14

    ROLE_CHOICES = (
        (IT_PROGRAMMING, 'IT and programming'),
        (ARCHITECTURE_ENGINEERING, 'Architecture and engineering'),
        (ARTS_CULTURE_ENTERTAINMENT, 'Arts, culture and entertainment'),
        (BUSINESS_MANAGEMENT_ADMINISTRATION, 'Business, management and administration'),
        (COMMUNICATIONS, 'Communications'),
        (COMMUNITY_SOCIALSERVICES, 'Community and social services'),
        (EDUCATION, 'Education'),
        (SCIENCE_TECHNOLOGY, 'Science and technology'),
        (INSTALLATION_REPAIR_MAINTENANCE, 'Installation, repair and maintenance'),
        (FARMING_FISHING_FORESTRY, 'Farming, fishing and forestry'),
        (GOVERNMENT, 'Government'),
        (HEALTH_MEDICINE, 'Health and medicine'),
        (LAW_PUBLICPOLICY, 'Law and public policy'),
        (SALES, 'Sales'),
    )

    field = models.PositiveSmallIntegerField(choices=ROLE_CHOICES)

    # def __str__(self):
    #     return self.get_id_display()


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    role = models.CharField(max_length=2, choices=ROLES)
    career_fields = models.ManyToManyField(CareerField, related_name='users')

    def __str__(self):
        return self.email
