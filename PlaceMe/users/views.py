from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from PlaceMe.permissions import IsUsersOrReadOnly
from PlaceMe.users.models import CustomUser
from PlaceMe.users.serializers import EmployeeRegistrationSerializer, EmployerRegistrationSerializer, \
    UserCareerSerializer

status_code201 = status.HTTP_201_CREATED
response201 = {
    'success': 'True',
    'status code': status_code201,
    'message': 'User registered  successfully',
}


class EmployeeRegistrationView(CreateAPIView):
    serializer_class = EmployeeRegistrationSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(response201, status=status_code201)


class EmployerRegistrationView(CreateAPIView):
    serializer_class = EmployerRegistrationSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(response201, status=status_code201)


class CareerFieldView(RetrieveUpdateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserCareerSerializer
    permission_classes = [IsUsersOrReadOnly]
