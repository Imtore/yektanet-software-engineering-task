from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from PlaceMe.users.views import *

urlpatterns = [
    path('signup/employee', EmployeeRegistrationView.as_view()),
    path('signup/employer', EmployerRegistrationView.as_view()),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
