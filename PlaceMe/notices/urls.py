from django.urls import path

from PlaceMe.notices.views import *

urlpatterns = [
    path('', SubmitNoticesView.as_view()),
    path('filter', FilteredListView.as_view()),
    path('<int:pk>', NoticesDetailView.as_view())

]
