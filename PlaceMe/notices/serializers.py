from rest_framework import serializers

from PlaceMe.notices.models import RecruitmentNotice
from PlaceMe.users.models import CustomUser
from PlaceMe.users.serializers import CareerFieldSerializer, update_career_fields


class NoticesSerializer(serializers.ModelSerializer):
    career_fields = CareerFieldSerializer(many=True, read_only=True)

    class Meta:
        model = RecruitmentNotice
        fields = ['owner', 'title', 'expiration_date', 'start_time', 'end_time', 'salary', 'career_fields']

    def create(self, validated_data):
        owner = validated_data.get('owner')
        owner = CustomUser.objects.get(pk=owner.id)
        notice = RecruitmentNotice.objects.create(
            owner=owner,
            title=validated_data.get('title'),
            expiration_date=validated_data.get('expiration_date'),
            start_time=validated_data.get('start_time'),
            end_time=validated_data.get('end_time'),
            salary=validated_data.get('salary')
        )
        return notice


class NoticesDetailSerializer(serializers.ModelSerializer):
    career_fields = CareerFieldSerializer(many=True)

    class Meta:
        model = RecruitmentNotice
        fields = ['id', 'owner', 'title', 'expiration_date', 'start_time', 'end_time', 'salary', 'career_fields']

    def update(self, instance, validated_data):
        career_fields = validated_data.pop('career_fields')
        instance = super().update(instance, validated_data)
        instance = update_career_fields(instance, career_fields)
        return instance
