class Node(object):
    def __init__(self, notice):
        self.data = notice.salary
        self.doubles = [notice]
        self.left = None
        self.right = None

    def insert(self, notice):
        d = notice.salary
        if self.data == d:
            self.doubles.append(notice)
            return False

        elif d < self.data:
            if self.left:
                return self.left.insert(notice)
            else:
                self.left = Node(notice)
                return True
        else:
            if self.right:
                return self.right.insert(notice)
            else:
                self.right = Node(notice)
                return True

    def inorder(self, a, b, l):
        x = self.data

        if a < x:
            if self.left:
                self.left.inorder(a, b, l)

        if a <= x <= b:
            l.extend(self.doubles)

        if b > x:
            if self.right:
                self.right.inorder(a, b, l)
        return l


class BST(object):
    def __init__(self):
        self.root = None

    def insert(self, notice):
        if self.root:
            return self.root.insert(notice)
        else:
            self.root = Node(notice)
            return True


    def inorder(self, a, b):
        if self.root:
            return self.root.inorder(a, b, [])
        else:
            return []

