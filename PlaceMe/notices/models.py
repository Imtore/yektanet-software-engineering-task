from django.db import models

from PlaceMe.users.models import CustomUser, CareerField


class RecruitmentNotice(models.Model):
    owner = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name="notices"
    )

    title = models.CharField(max_length=100)
    expiration_date = models.DateTimeField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    salary = models.PositiveIntegerField()
    career_fields = models.ManyToManyField(CareerField, related_name='notices')
    # gender
    # description
    # full-time-part-time
    # # of days of week
    # lunch



