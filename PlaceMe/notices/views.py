from django.core.cache import cache

from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView

from PlaceMe.notices.models import RecruitmentNotice
from PlaceMe.permissions import IsOwnerOrReadOnly, IsEmployerOrReadOnly, IsEmployee
from PlaceMe.notices.serializers import NoticesSerializer, NoticesDetailSerializer
from PlaceMe.notices.bst import BST


class SubmitNoticesView(CreateAPIView):
    queryset = RecruitmentNotice.objects.all()
    serializer_class = NoticesSerializer
    permission_classes = [IsEmployerOrReadOnly]


class FilteredListView(ListAPIView):
    serializer_class = NoticesSerializer
    permission_classes = [IsEmployee]

    def get_queryset(self):
        key = 'all_notices'
        # notices = cache.get(key)
        # if notices is None:
        notices = RecruitmentNotice.objects.all()
        #     cache.set(key, notices, 1800)

        l_price = int(self.request.query_params['l-price'])
        m_price = int(self.request.query_params['m-price'])

        bst = BST()
        for notice in notices:
            bst.insert(notice)

        return bst.inorder(l_price, m_price)


class NoticesDetailView(RetrieveUpdateDestroyAPIView):
    queryset = RecruitmentNotice.objects.all()
    serializer_class = NoticesDetailSerializer
    permission_classes = [IsOwnerOrReadOnly]
