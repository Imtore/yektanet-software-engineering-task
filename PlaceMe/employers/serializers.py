from rest_framework import serializers

from PlaceMe.employers.models import EmployerProfile


class EmployerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployerProfile
        fields = ['id', 'owner', 'company_name', 'established_year', 'address', 'phone_number']



