from rest_framework import permissions
from rest_framework.generics import RetrieveUpdateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView

from PlaceMe.employers.models import EmployerProfile
from PlaceMe.employers.serializers import EmployerProfileSerializer
from PlaceMe.permissions import IsOwnerOrReadOnly


class EmployerProfileDetailView(RetrieveUpdateAPIView):
    queryset = EmployerProfile.objects.all()
    serializer_class = EmployerProfileSerializer
    permission_classes = [IsOwnerOrReadOnly]


class EmployerProfilesListView(ListAPIView):
    queryset = EmployerProfile.objects.all()
    serializer_class = EmployerProfileSerializer
    permission_classes = [permissions.IsAuthenticated]




