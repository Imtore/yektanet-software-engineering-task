from django.urls import path
from PlaceMe.employers.views import EmployerProfileDetailView, EmployerProfilesListView

urlpatterns = [
    path('profile/<int:pk>', EmployerProfileDetailView.as_view()),
    path('profiles', EmployerProfilesListView.as_view()),
]
