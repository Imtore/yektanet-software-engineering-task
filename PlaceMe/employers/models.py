from django.db import models

from PlaceMe.users.models import CustomUser, CareerField


class EmployerProfile(models.Model):
    owner = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, related_name="employer_account"
    )

    company_name = models.CharField(max_length=100)
    established_year = models.PositiveSmallIntegerField(null=False, blank=False)
    address = models.CharField(max_length=500)
    phone_number = models.CharField(max_length=11)


