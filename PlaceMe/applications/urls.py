from django.urls import path

from PlaceMe.applications.views import *

urlpatterns = [
    path('', SubmitApplicationView.as_view())
]