from rest_framework.generics import CreateAPIView

from PlaceMe.permissions import IsEmployeeOrReadOnly
from PlaceMe.applications.serializers import ApplicationSerializer


class SubmitApplicationView(CreateAPIView):
    serializer_class = ApplicationSerializer
    permission_classes = [IsEmployeeOrReadOnly]
