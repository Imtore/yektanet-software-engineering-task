from django.db import models

from PlaceMe.notices.models import RecruitmentNotice
from PlaceMe.users.models import CustomUser


class JobApplication(models.Model):
    owner = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name="applications"
    )
    notice = models.ForeignKey(RecruitmentNotice, on_delete=models.CASCADE)
