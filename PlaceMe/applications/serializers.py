from rest_framework import serializers

from PlaceMe.applications.models import JobApplication
from PlaceMe.notices.models import RecruitmentNotice
from PlaceMe.users.models import CustomUser


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobApplication
        fields = ['owner', 'notice']

    def create(self, validated_data):
        owner = CustomUser.objects.get(pk=validated_data.get('owner').id)
        notice = RecruitmentNotice.objects.get(pk=validated_data.get('notice').id)
        application = JobApplication.objects.create(
            owner=owner,
            notice=notice
        )

        return application
